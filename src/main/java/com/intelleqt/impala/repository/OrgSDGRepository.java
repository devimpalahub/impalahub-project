/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

import com.intelleqt.impala.models.OrgSDG;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author SMWAURA1
 */
public interface OrgSDGRepository extends JpaRepository<OrgSDG, Long> {
    
    @Transactional
    @Modifying
    @Query("DELETE OrgSDG o where o.org_id=?1")
    void deleteOrgSDGsByOrgId(Long org_id);
}
