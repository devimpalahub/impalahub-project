/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.utils;

import java.io.IOException;

/**
 *
 * @author SMWAURA1
 */
public final class DatabaseUtil {

    public static boolean backup(String dbUsername, String dbPassword, String dbName, String outputFile) throws IOException, InterruptedException {
        String command = "";
        if (!dbPassword.replace(" ", "").isEmpty()) {
            command = String.format("docker exec -i mysql_db mysqldump -u%s -p%s --add-drop-table --databases %s > %s", dbUsername, dbPassword, dbName, outputFile);
        } else {
            command = String.format("docker exec -i mysql_db mysqldump  -u%s --add-drop-table --databases %s > %s", dbUsername, dbName, outputFile);
        }
        Process process = Runtime.getRuntime().exec(command);
        int processComplete = process.waitFor();
        return processComplete == 0;
    }
}
