/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.Organization;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Random;
import net.minidev.json.JSONObject;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author JNWARUI
 */
@RestController
public class FileController extends BaseController {

    @RequestMapping(value = "/organizations/uploads/uploadFile", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Object> handleFileUpload(@RequestBody Organization org) throws IOException {

        String remoteAccessPath = env.getProperty("file.remote.access.path");

        String remoteAccessPathToSave = env.getProperty("baseurl") + remoteAccessPath + org.getFilename();

        Optional<Organization> organization = organizationRepository.findById(org.getOrg_id());

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization id not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            organizationRepository.updateOrgLogoLink(remoteAccessPathToSave, org.getOrg_id());
        }

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("responseCode", "200");
        jsonObj.put("responseMessage", "Success");

        return new ResponseEntity<>(jsonObj, HttpStatus.OK);
    }

    @RequestMapping(value = "/organizations/uploads/uploadFile2", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Object> handleFileUpload2(@RequestParam(value = "file") MultipartFile file, @RequestParam(required = false) Long org_id) throws IOException {

        String uploadDir = env.getProperty("file.upload.dir");

        String remoteAccessPath = env.getProperty("file.remote.access.path");

        String fileExtension = getFileExtension(file);
        String filename = file.getOriginalFilename();

        File targetFile = getTargetFile(fileExtension, filename, uploadDir);

//        byte[] bytes = file.getBytes();
        file.transferTo(targetFile);
//        String UploadedDirectory = targetFile.getAbsolutePath();
        String remoteAccessPathToSave = env.getProperty("baseurl") + remoteAccessPath + filename;

        Organization organization = organizationRepository.findById(org_id).orElse(null);

        if (organization.equals(null)) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization id not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            organizationRepository.updateOrgLogoLink(remoteAccessPathToSave, org_id);
        }

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("responseCode", "200");
        jsonObj.put("responseMessage", "Success");

        return new ResponseEntity<>(jsonObj, HttpStatus.OK);
    }

    @GetMapping("/organizations/uploads/downloadFile/logo/{id}")
    public ResponseEntity<Object> download(@PathVariable Long id) throws IOException {

        Optional<Organization> organization = organizationRepository.findById(id);
        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "400");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {

            Resource resource = null;

            if (organization.get().getLogolink() != null && !organization.get().getLogolink().isEmpty()) {

                try {
                    resource = documentStorageService.loadFileAsResource(organization.get().getLogolink());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);

            } else {

                return ResponseEntity.notFound().build();

            }
        }

    }

    private String getRandomString() {
        return new Random().nextInt(999999) + "_" + System.currentTimeMillis();
    }

    private File getTargetFile(String fileExtn, String fileName, String uploadDir) {
        File targetFile = new File(uploadDir + fileName);
        return targetFile;
    }

    private String getFileExtension(MultipartFile inFile) {
        String fileExtention = inFile.getOriginalFilename().substring(inFile.getOriginalFilename().lastIndexOf('.'));
        return fileExtention;
    }
}
