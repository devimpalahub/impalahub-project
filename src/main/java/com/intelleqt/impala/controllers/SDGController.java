/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.SDG;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class SDGController extends BaseController {

    @RequestMapping(value = "/sdg/all", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        List<SDG> list = (List<SDG>) sdgRepository.findAll();
//        return new ResponseEntity<>(list, HttpStatus.OK);
        if (list.isEmpty()) {
            //log.error("Id " + id + " is not existed");
//            ResponseEntity.notFound().build();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No SDG found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONArray jArray = new JSONArray();
            for (SDG sdg : list) {
                JSONObject ctyJSON = new JSONObject();
                ctyJSON.put("sdg_id", sdg.getSdg_id());
                ctyJSON.put("sdg_name", sdg.getSdg_name());
                jArray.add(ctyJSON);
            }

            jsonObj.put("data", jArray);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/sdg/add")
    public ResponseEntity<SDG> createSDG(@RequestBody SDG sgd) {
        SDG sdg = new SDG();
        sdg.setSdg_name(sgd.getSdg_name());
        return new ResponseEntity<>(sdgRepository.save(sdg), HttpStatus.OK);
    }

    @GetMapping("/sdg/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Optional<SDG> msg = sdgRepository.findById(id);
        if (!msg.isPresent()) {
            //log.error("Id " + id + " is not existed");
//            ResponseEntity.notFound().build();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "SDG not found");
            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            JSONObject data = new JSONObject();
            data.put("sdg_id", msg.get().getSdg_id());
            data.put("sdg_name", msg.get().getSdg_name());
            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @DeleteMapping("/sdg/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!sdgRepository.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }
        sdgRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
