/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author JNWARUI
 */
@Entity
@Table(name = "tbl_organization")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class Organization {

    //@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long org_id;
    
    private String orgname;
    private String email;
    private String phonenumber;
    private String username;
    private String password;
    private String logolink;
    private String registration_token;
    private String orgtype;
    private String website_url;
    private String is_admin;
    private String chat_id;
    private String chat_token;
    private String full_names;
    private String designation;
    private String consent;
    private String consent_details;
    
    @CreationTimestamp
    private Timestamp trx_date;
    
    @Transient
    private String cty_id;
    @Transient
    private String country_name;
    @Transient
    private List<Country> countries;
    @Transient
    private List<SDG> sdgs;
    @Transient
    private List<FocusArea> focusAreas;
    @Transient
    private String new_password;
    @Transient
    private String confirm_new_password;
    private int need_password_reset;    
    @JsonIgnore
    private String active_status;
    @Transient
    private String responseCode;
    @Transient
    private String responseMessage;
    @Transient
    private String filename;

    public String getCty_id() {
        return cty_id;
    }

    public void setCty_id(String cty_id) {
        this.cty_id = cty_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "org_id", referencedColumnName = "org_id",nullable=false ,insertable=false, updatable=false)
    private List<OrgCountry> orgCountries;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "org_id", referencedColumnName = "org_id",nullable=false ,insertable=false, updatable=false)
    private List<OrgSDG> orgSdg;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "org_id", referencedColumnName = "org_id",nullable=false ,insertable=false, updatable=false)
    private List<OrgFocusArea> orgFocusAreas;

    public List<OrgCountry> getOrgCountries() {
        return orgCountries;
    }

    public void setOrgCountries(List<OrgCountry> orgCountries) {
        this.orgCountries = orgCountries;
    }

    public String getOrgtype() {
        return orgtype;
    }

    public void setOrgtype(String orgtype) {
        this.orgtype = orgtype;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }
    
    public int getNeed_password_reset() {
        return need_password_reset;
    }

    public void setNeed_password_reset(int need_password_reset) {
        this.need_password_reset = need_password_reset;
    }
    
    public String getActive_status() {
        return active_status;
    }

    public void setActive_status(String active_status) {
        this.active_status = active_status;
    }

    public String getRegistration_token() {
        return registration_token;
    }

    public void setRegistration_token(String registration_token) {
        this.registration_token = registration_token;
    }
    
    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }
    
    //private String registration_token;
    //private String active_status;
    //private String trx_date;
    //private String last_update;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    
    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogolink() {
        return logolink;
    }

    public void setLogolink(String logolink) {
        this.logolink = logolink;
    }

    @OneToMany(mappedBy = "organizations")
    @JsonIgnore
    private List<Organization> organizations;

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }

    public Long getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Long org_id) {
        this.org_id = org_id;
    }

}
