/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.services;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

/**
 *
 * @author JNWARUI
 */
@Service
public class DocumentStorageService {
    private Path fileStorageLocation;
    
    public Resource loadFileAsResource(String fileName) throws Exception {

        try {

//            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Path filePath = Paths.get(fileName);

            Resource resource = new UrlResource(filePath.toUri());

            if (resource.exists()) {

                return resource;

            } else {

                throw new FileNotFoundException("File not found " + fileName);

            }

        } catch (MalformedURLException ex) {

            throw new FileNotFoundException("File not found " + fileName);

        }

    }

}
