/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

/**
 *
 * @author SMWAURA1
 */
import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_organization_types")
@Data
public class OrgType {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long org_typ_id;
    private String type_name;
    @CreationTimestamp
    private Timestamp trx_date;
}
