/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.Organization;
import java.util.Optional;
import java.util.Random;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JNWARUI
 */
@RestController
public class PasswordController extends BaseController {

    @PostMapping("/organizations/password/set")
    public ResponseEntity<Object> setPassword(@RequestBody Organization org) {
        if (!org.getNew_password().equals(org.getConfirm_new_password())) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "400");
            jsonObj.put("responseMessage", "New Password and New Password Confirmation values do not match");

            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
        }
        Optional<Organization> organization = organizationRepository.findUserByUsername(org.getUsername());

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization with username" + org.getUsername() + " not found!");

            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
        } else {

            organizationRepository.updateForgotPassword(passwordEncoder.get_SHA_512_SecurePassword(org.getNew_password()), 0, organization.get().getOrg_id());
            String token = passwordEncoder.get_SHA_512_SecurePassword(new Random().nextInt(999999) + "_" + System.currentTimeMillis());
            organizationRepository.updateRegistrationToken(token, organization.get().getOrg_id());

            JSONObject jsonObj = new JSONObject();

            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONObject data = new JSONObject();
            data.put("registration_token", token);

            jsonObj.put("data", data);

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @PostMapping("/organizations/password/change")
    public ResponseEntity<Object> changePassword(@RequestBody Organization org) {
        if (!org.getNew_password().equals(org.getConfirm_new_password())) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "400");
            jsonObj.put("responseMessage", "New Password and New Password Confirmation values do not match");
            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
        }
        Optional<Organization> organization = organizationRepository.findUserByUsernameAndPassword(org.getUsername(), passwordEncoder.get_SHA_512_SecurePassword(org.getPassword()));
        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "401");
            jsonObj.put("responseMessage", "Invalid credentials provided");
            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
        } else {
            organizationRepository.updateForgotPassword(passwordEncoder.get_SHA_512_SecurePassword(org.getNew_password()), 0, organization.get().getOrg_id());
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Password successfully reset");
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @PostMapping("/organizations/password/forgot")
    public ResponseEntity<Object> resetPassword(@RequestBody Organization org) {
        Optional<Organization> organization = organizationRepository.findUserByUsername(org.getUsername());

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            int random = new Random().nextInt(999999);
            organizationRepository.updateForgotPassword(passwordEncoder.get_SHA_512_SecurePassword(String.valueOf(random)), 1, organization.get().getOrg_id());

            String emailBody = "Hi, Please use this OTP as your password to login and set your new password: <b>" + String.valueOf(random) + "</b>";
            //Send Activation Email
            emailSender.sendMailWithHTML("info@impalahub.com", "ImpalaHub", organization.get().getEmail(), "Password Reset", emailBody);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Password successfully reset. New password sent to email");

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }
}
