/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

import com.intelleqt.impala.models.Organization;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author JNWARUI
 */
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    
    @Query("SELECT u FROM Organization u WHERE u.username = ?1")
    Optional<Organization> findUserByUsername(String username);
    
    @Query("SELECT u FROM Organization u WHERE u.username = ?1 and u.password = ?2")
    Optional<Organization> findUserByUsernameAndPassword(String username, String password);
    
    @Query("SELECT u FROM Organization u WHERE u.registration_token = ?1")
    Optional<Organization> findUserByToken(String token);
    
//    @Query("SELECT u FROM Organization u INNER JOIN OrgCountry o ON u.org_id=o.org_id INNER JOIN \n" +
//"Country c ON o.cty_id=c.cty_id WHERE u.org_id=?1") 
    @Query("SELECT u FROM Organization u INNER JOIN OrgCountry o ON u.org_id=o.org_id WHERE u.org_id=?1") 
    Optional<Organization> findOrgById(Long org_id);
    
    @Query("SELECT u FROM Organization u WHERE u.active_status = ?1")
    List<Organization> findOrgByActiveStatus(String active_status);
    
    //Activate org account
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.active_status = ?1 where u.org_id=?2")
    void activateOrg(String status, Long org_id);
    
    //DeActivate org account
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.active_status = ?1 where u.org_id=?2")
    void deActivateOrg(String status, Long org_id);
    
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.logolink = ?1 where u.org_id=?2")
    void updateOrgLogoLink(String logolink, Long org_id);
    
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.password = ?1 where u.org_id=?2")
    void updatePassword(String password, Long org_id);
    
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.password = ?1,u.need_password_reset = ?2 where u.org_id=?3")
    void updateForgotPassword(String password,int need_password_reset,  Long org_id);
    
    @Transactional
    @Modifying
    @Query("UPDATE Organization u set u.registration_token = ?1 where u.org_id=?2")
    void updateRegistrationToken(String registration_token, Long org_id);
}
