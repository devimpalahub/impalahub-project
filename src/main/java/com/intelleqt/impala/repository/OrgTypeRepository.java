/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

/**
 *
 * @author SMWAURA1
 */
import com.intelleqt.impala.models.OrgType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrgTypeRepository extends JpaRepository<OrgType, Long> {

}
