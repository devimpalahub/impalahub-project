/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

/**
 *
 * @author SMWAURA1
 */
import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_org_focusareas")
@Data
public class OrgFocusArea {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long org_focusarea_id;
    
    @ManyToOne
    @JoinColumn(name = "fa_id",nullable=false ,insertable=false, updatable=false)
    FocusArea focusArea;
    
    @Column(name="fa_id")
    private Long fa_id;
    
    @ManyToOne
    @JoinColumn(name = "org_id",nullable=false,insertable=false, updatable=false)
    Organization org;
    
    @Column(name="org_id")
    private Long org_id;
    
    @CreationTimestamp
    private Timestamp trx_date;
}
