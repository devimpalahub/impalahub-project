/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

/**
 *
 * @author SMWAURA1
 */
import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_org_sdgs")
@Data

public class OrgSDG {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long org_sdg_id;
    
    @ManyToOne
    @JoinColumn(name = "sdg_id",nullable=false ,insertable=false, updatable=false)
    SDG sdg;
    
    @Column(name="sdg_id")
    private Long sdg_id;
    
    @ManyToOne
    @JoinColumn(name = "org_id",nullable=false,insertable=false, updatable=false)
    Organization org;
    
    @Column(name="org_id")
    private Long org_id;
    
    @CreationTimestamp
    private Timestamp trx_date;

}
