/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.utils;

import static com.google.common.collect.Lists.newArrayList;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 *
 * @author JNWARUI
 */
@Component
public class EmailSender {

    @Autowired
    private EmailService emailService;
    
    @Autowired
    private JavaMailSender mailSender;

//    public void setMailSender(JavaMailSender mailSender) {
//        this.mailSender = mailSender;
//    }

    public void sendmail(String fromEmail, String fromName, String to, String subject, String body) throws UnsupportedEncodingException {

        final DefaultEmail email = DefaultEmail.builder()
                .from(new InternetAddress(fromEmail, fromName))
                .to(newArrayList(new InternetAddress(to, "")))
                .subject(subject)
                .body(body)
                .encoding("UTF-8").build();
        emailService.send(email);
    }

    public void sendMailWithHTML(String fromEmail, String fromName, String to, String subject, String body) {

        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
//            String htmlMsg = "<h3>Hello World!</h3><p></p>";
            //mimeMessage.setContent(htmlMsg, "text/html"); /** Use this or below line **/
            helper.setText(body, true); // Use this or above line.
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom(new InternetAddress(fromEmail, fromName));
            mailSender.send(mimeMessage);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
