/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.FocusArea;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class FocusAreaController extends BaseController {

    @RequestMapping(value = "/focusarea/all", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        List<FocusArea> list = (List<FocusArea>) faRepository.findAll();
        if (list.isEmpty()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No Focus Area found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONArray jArray = new JSONArray();
            for (FocusArea focusArea : list) {
                JSONObject faJSON = new JSONObject();
                faJSON.put("fa_id", focusArea.getFa_id());
                faJSON.put("focus_name", focusArea.getFocus_name());
                jArray.add(faJSON);
            }

            jsonObj.put("data", jArray);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/focusarea/add")
    public ResponseEntity<FocusArea> createFocusArea(@RequestBody FocusArea foa) {
        FocusArea country = new FocusArea();
        country.setFocus_name(foa.getFocus_name());
        return new ResponseEntity<>(faRepository.save(country), HttpStatus.OK);
    }

    @GetMapping("/focusarea/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Optional<FocusArea> msg = faRepository.findById(id);
        if (!msg.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Focus area not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONObject data = new JSONObject();
            data.put("fa_id", msg.get().getFa_id());
            data.put("focus_name", msg.get().getFocus_name());

            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @DeleteMapping("/focusarea/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!faRepository.findById(id).isPresent()){
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }
        faRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
