/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

//import org.springframework.web.bind.annotation.GetMapping;
import com.intelleqt.impala.utils.DatabaseUtil;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */

@RestController
public class DownloadController extends BaseController{

    @GetMapping(value = "/download/backup")
    public ResponseEntity<Object> backUp() {

        String dbUsername = env.getProperty("spring.datasource.username");
        String dbPassword = env.getProperty("spring.datasource.password");
        String dbName = "impala_hub";//env.getProperty("file.remote.access.path");
        String outputFile = env.getProperty("file.upload.dir")+env.getProperty("file.sql.dump");

        try {
            boolean bool = DatabaseUtil.backup(dbUsername, dbPassword, dbName, outputFile);
            if (bool) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("responseCode", "200");
                jsonObj.put("responseMessage", "Success. Backup Complete...");
                return new ResponseEntity<>(jsonObj, HttpStatus.OK);
            } else {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("responseCode", "500");
                jsonObj.put("responseMessage", "Failed. Backup Failure...");
                return new ResponseEntity<>(jsonObj, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "500");
            jsonObj.put("responseMessage", "An error has occurred " + e.toString());
            return new ResponseEntity<>(jsonObj, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
