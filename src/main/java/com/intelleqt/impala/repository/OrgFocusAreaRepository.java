/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

/**
 *
 * @author SMWAURA1
 */

import com.intelleqt.impala.models.OrgFocusArea;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OrgFocusAreaRepository extends JpaRepository<OrgFocusArea, Long> {
    
    @Transactional
    @Modifying
    @Query("DELETE OrgFocusArea o where o.org_id=?1")
    void deleteOrgFocusAreaByOrgId(Long org_id);
}
