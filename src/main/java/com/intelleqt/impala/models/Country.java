/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author JNWARUI
 */
@Entity
@Table(name = "tbl_countries")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


public class Country {

    //@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long cty_id;
    
    private String country_name;

    public Long getCty_id() {
        return cty_id;
    }

    public void setCty_id(Long cty_id) {
        this.cty_id = cty_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    

}
