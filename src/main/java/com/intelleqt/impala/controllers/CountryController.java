/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.Country;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class CountryController extends BaseController{
    @RequestMapping(value = "/country/all", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        List<Country> list = (List<Country>) ctryRepository.findAll();
        //return new ResponseEntity<>(list, HttpStatus.OK);
        if (list.isEmpty()) {
            //log.error("Id " + id + " is not existed");
//            ResponseEntity.notFound().build();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No country found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            
            JSONArray jArray = new JSONArray();
            for (Country cty : list) {
                JSONObject ctyJSON = new JSONObject();
                ctyJSON.put("cty_id", cty.getCty_id());
                ctyJSON.put("country_name", cty.getCountry_name());
                jArray.add(ctyJSON);
            }

            jsonObj.put("data", jArray);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/country/add")
    public ResponseEntity<Country> createCountry(@RequestBody Country ctry) {
        Country country = new Country();
        country.setCountry_name(ctry.getCountry_name());
        return new ResponseEntity<>(ctryRepository.save(country), HttpStatus.OK);
    }

    @GetMapping("/country/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Optional<Country> msg = ctryRepository.findById(id);
        if (!msg.isPresent()) {
            //log.error("Id " + id + " is not existed");
//            ResponseEntity.notFound().build();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Country not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONObject data = new JSONObject();
            data.put("cty_id", msg.get().getCty_id());
            data.put("country_name", msg.get().getCountry_name());

            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @DeleteMapping("/country/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!ctryRepository.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }
        ctryRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
