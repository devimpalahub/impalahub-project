/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.OrgType;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class OrgTypeController extends BaseController {

    @RequestMapping(value = "/orgtype/all", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        List<OrgType> list = (List<OrgType>) orgtypeRepository.findAll();

        //return new ResponseEntity<>(list, HttpStatus.OK);
        if (list.isEmpty()) {
            //log.error("Id " + id + " is not existed");
//            ResponseEntity.notFound().build();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No OrgType found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONArray jArray = new JSONArray();
            for (OrgType otype : list) {
                JSONObject ctyJSON = new JSONObject();
                ctyJSON.put("org_typ_id", otype.getOrg_typ_id());
                ctyJSON.put("type_name", otype.getType_name());
                jArray.add(ctyJSON);
            }

            jsonObj.put("data", jArray);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/orgtype/add")
    public ResponseEntity<OrgType> createOrgType(@RequestBody OrgType otype) {
        OrgType orgtype = new OrgType();
        orgtype.setType_name(otype.getType_name());
        return new ResponseEntity<>(orgtypeRepository.save(orgtype), HttpStatus.OK);
    }

    @GetMapping("/orgtype/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Optional<OrgType> msg = orgtypeRepository.findById(id);
        if (!msg.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "OrgType not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            JSONObject data = new JSONObject();
            data.put("org_typ_id", msg.get().getOrg_typ_id());
            data.put("type_name", msg.get().getType_name());
            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @DeleteMapping("/orgtype/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!orgtypeRepository.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }
        orgtypeRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
