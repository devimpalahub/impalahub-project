/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import org.springframework.core.env.Environment;
import com.intelleqt.impala.repository.CountryRepository;
import com.intelleqt.impala.repository.FocusAreaRepository;
import com.intelleqt.impala.repository.MessageRepository;
import com.intelleqt.impala.repository.OrgCountryRepository;
import com.intelleqt.impala.repository.OrgFocusAreaRepository;
import com.intelleqt.impala.repository.OrgSDGRepository;
import com.intelleqt.impala.repository.OrgTypeRepository;
import com.intelleqt.impala.repository.OrganizationRepository;
import com.intelleqt.impala.repository.SDGRepository;
import com.intelleqt.impala.services.DocumentStorageService;
import com.intelleqt.impala.utils.EmailSender;
import com.intelleqt.impala.utils.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
@RequestMapping("/api/v1/impalahub")
public class BaseController {

    @Autowired
    protected Environment env;
    @Autowired
    protected CountryRepository ctryRepository;
    @Autowired
    protected OrganizationRepository organizationRepository;
    @Autowired
    protected DocumentStorageService documentStorageService;
    @Autowired
    protected FocusAreaRepository faRepository;
    @Autowired
    protected MessageRepository msgRepository;
    @Autowired
    protected OrgCountryRepository orgCountryRepository;
    @Autowired
    protected OrgFocusAreaRepository orgFocusAreaRepository;
    @Autowired
    protected OrgSDGRepository orgSdgRepository;
    @Autowired
    protected OrgTypeRepository orgtypeRepository;
    @Autowired
    protected PasswordEncoder passwordEncoder;
    @Autowired
    protected EmailSender emailSender;
    @Autowired
    protected SDGRepository sdgRepository;
}
