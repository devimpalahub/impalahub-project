/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.OrgCountry;
import com.intelleqt.impala.models.Organization;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class OrgCountriesController extends BaseController {

    @Transactional
    @PutMapping(value = "/organizations/countries/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody List<OrgCountry> OrgCountries) {

        Optional<Organization> organization = organizationRepository.findById(id);

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);

        } else {
            //Delete all org sgds
            orgCountryRepository.deleteOrgCountriesByOrgId(id);
            //Save New SDGs
            orgCountryRepository.saveAll(OrgCountries);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Organization Countries updated successfully");

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }
}
