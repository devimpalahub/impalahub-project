package com.intelleqt.impala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan(basePackages = {"com.intelleqt.impala","it.ozimov.springboot"})
@EnableAsync
public class ImpalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImpalaApplication.class, args);
	}

}
