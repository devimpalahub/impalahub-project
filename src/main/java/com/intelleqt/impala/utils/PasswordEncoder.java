/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import org.springframework.stereotype.Component;

/**
 *
 * @author JNWARUI
 */
@Component
public class PasswordEncoder {
    
    private final String SALT="1mp@laHub@2021!!!";
    
//    public String encode(String plainPassword) {
//        int strength = 10;
//        BCryptPasswordEncoder bCryptPasswordEncoder
//                = new BCryptPasswordEncoder(strength, new SecureRandom());
//        return bCryptPasswordEncoder.encode(plainPassword);
//    }

    public String get_SHA_512_SecurePassword(String passwordToHash) {
        String generatedPassword = null;
        byte[] salt=SALT.getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
    
    //Add salt
    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
}
