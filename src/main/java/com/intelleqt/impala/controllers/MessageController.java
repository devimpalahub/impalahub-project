/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.Message;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SMWAURA1
 */
@RestController
public class MessageController extends BaseController {
    @Transactional
    @PostMapping(value = "/message/add")
    public ResponseEntity<Message> createMessage(@RequestBody Message msg) {
        Message msgObj = new Message();
        msgObj.setFrom_org_id(msg.getFrom_org_id());
        msgObj.setTo_org_id(msg.getTo_org_id());
        msgObj.setSubject(msg.getSubject());
        msgObj.setMessage(msg.getMessage());
        msgObj.setStatus_read(Long.parseLong("0"));
        return new ResponseEntity<>(msgRepository.save(msgObj), HttpStatus.OK);
    }

    @GetMapping("/message/{id}")
    public ResponseEntity<Message> findById(@PathVariable Long id) {
        Optional<Message> msg = msgRepository.findById(id);
        if (!msg.isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(msg.get());
    }

    @DeleteMapping("/message/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!msgRepository.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.notFound().build();
        }
        msgRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/message/{id}/{status}")
    public ResponseEntity<Object> getAll(@PathVariable Long id, @PathVariable String status) {
        List<Message> list = null;

        if (status.trim().isEmpty() || status.equals("") || status.equals("all")) {
            list = msgRepository.findAllMessagesByID(id);
        } else if (status.equals("read") || status.equals("unread")) {
            String strStatus = status.equals("read") ? "1" : "0";
            list = msgRepository.findMessageStatusByID(id, Long.parseLong(strStatus));
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "400");
            jsonObj.put("responseMessage", "Invalid status requested");
            return new ResponseEntity<>(jsonObj, HttpStatus.BAD_REQUEST);
        }
        if (list.isEmpty() || list == null) {

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No messages found");
            jsonObj.put("data", new JSONArray());

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONArray jArray = new JSONArray();
            for (Message msg : list) {
                JSONObject msgJSON = new JSONObject();
                msgJSON.put("msg_id", msg.getMsg_id());
                msgJSON.put("org_id", msg.getTo_org_id());
                msgJSON.put("msg_subject", msg.getSubject());
                msgJSON.put("msg_from", organizationRepository.findById(msg.getFrom_org_id()).get().getOrgname());
                msgJSON.put("status_read",msg.getStatus_read());
                msgJSON.put("message", msg.getMessage());
                jArray.add(msgJSON);
            }
            jsonObj.put("data", jArray);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/message/markasread")
    public ResponseEntity<Object> markAsRead(@RequestBody Message msg) {
        msgRepository.markAsRead(msg.getMsg_id());
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("responseCode", "200");
        jsonObj.put("responseMessage", "Success");
        return new ResponseEntity<>(jsonObj, HttpStatus.OK);
    }

    @GetMapping("/message/unreadmsgcount/{id}")
    public ResponseEntity<Object> getAll(@PathVariable Long id) {
        List<Message> list = msgRepository.findMessageStatusByID(id, Long.parseLong("0"));
        if (list.isEmpty() || list.equals(null)) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No messages found");
            jsonObj.put("messageCount", 0);
            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            jsonObj.put("messageCount", list.size());
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }
}
