/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

/**
 *
 * @author SMWAURA1
 */
import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_org_countries")
@Data
public class OrgCountry {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long org_cty_id;
    
    @ManyToOne
    @JoinColumn(name = "cty_id",nullable=false ,insertable=false, updatable=false)
    Country country;
    
    @Column(name="cty_id")
    private Long cty_id;
    
    @ManyToOne
    @JoinColumn(name = "org_id",nullable=false,insertable=false, updatable=false)
    Organization org;
    
    @Column(name="org_id")
    private Long org_id;
    
    @CreationTimestamp
    private Timestamp trx_date;
}
