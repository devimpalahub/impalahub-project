/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.intelleqt.impala.models.Country;
import com.intelleqt.impala.models.OrgCountry;
import com.intelleqt.impala.models.OrgFocusArea;
import com.intelleqt.impala.models.OrgSDG;
import com.intelleqt.impala.models.Organization;
import com.intelleqt.impala.models.SDG;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import javax.transaction.Transactional;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JNWARUI
 */
@RestController
public class OrganizationController extends BaseController {

    @Transactional
    @PostMapping(value = "/organizations/register")
    public ResponseEntity<Object> createOrg(@RequestBody Organization org) {

        Optional<Organization> organization = organizationRepository.findUserByUsername(org.getUsername());
        if (organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "400");
            jsonObj.put("responseMessage", "Organization with username " + org.getUsername() + " already exists. Please use another username");
            return new ResponseEntity<>(jsonObj, HttpStatus.CONFLICT);
        } else {
            Organization orgObj = new Organization();
            orgObj.setOrgname(org.getOrgname());
            orgObj.setEmail(org.getEmail());
            orgObj.setPhonenumber(org.getPhonenumber());
            orgObj.setUsername(org.getUsername());
            orgObj.setWebsite_url(org.getWebsite_url());
            orgObj.setOrgtype(org.getOrgtype());
            orgObj.setPassword(passwordEncoder.get_SHA_512_SecurePassword(org.getPassword()));
            orgObj.setLogolink(org.getLogolink());
            orgObj.setActive_status("0");
            orgObj.setFull_names(org.getFull_names());
            orgObj.setDesignation(org.getDesignation());
            orgObj.setConsent("1");
            orgObj.setConsent_details(org.getConsent_details());
            //BaseURL + token
            String token = passwordEncoder.get_SHA_512_SecurePassword(new Random().nextInt(999999) + "_" + System.currentTimeMillis());
            orgObj.setRegistration_token(token);
//            String baseURLToken = env.getProperty("baseurl")+"/api/v1/organizations/activate/"+token;
            //Save details
            organizationRepository.save(orgObj);
            //Save Org countries
            for (Country cty : org.getCountries()) {
                OrgCountry orgCntry = new OrgCountry();
                orgCntry.setCty_id(cty.getCty_id());
                orgCntry.setOrg_id(orgObj.getOrg_id());
                orgCountryRepository.save(orgCntry);
            }

            //Save Org SDGs
            for (SDG sdg : org.getSdgs()) {
                OrgSDG orgSdg = new OrgSDG();
                orgSdg.setSdg_id(sdg.getSdg_id());
                orgSdg.setOrg_id(orgObj.getOrg_id());
                orgSdgRepository.save(orgSdg);
            }

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            JSONObject data = new JSONObject();
            data.put("org_id", orgObj.getOrg_id());
            data.put("username", orgObj.getUsername());
            data.put("registration_token", token);
            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/organizations/activationtoken/resend")
    public ResponseEntity<Object> resendActivationToken(@RequestBody Organization org) {
        Optional<Organization> organization = organizationRepository.findById(org.getOrg_id());
        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");
            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            //BaseURL + token
            String token = passwordEncoder.get_SHA_512_SecurePassword(new Random().nextInt(999999) + "_" + System.currentTimeMillis());
            organizationRepository.updateRegistrationToken(token, organization.get().getOrg_id());
            //Send Activation Email
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Activation token successfully sent");
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @GetMapping("/organizations/activate/{token}")
    public ResponseEntity<Object> activateOrg(@PathVariable String token) {
        Optional<Organization> organization = organizationRepository.findUserByToken(token);

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Token is invalid");

            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);

        } else {

            organizationRepository.activateOrg("1", organization.get().getOrg_id());

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Organization succesfully activated");

            JSONObject data = new JSONObject();

            data.put("email", organization.get().getEmail());

            jsonObj.put("data", data);

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @GetMapping("/organizations/activestatus/{id}")
    public ResponseEntity<Object> findByActiveStatus(@PathVariable String id) {
        List<Organization> orgList = (List<Organization>) organizationRepository.findOrgByActiveStatus(id);

        if (orgList.isEmpty()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No organization found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);

        } else {

            JSONObject jsonObj = new JSONObject();

            JSONArray jArray = new JSONArray();

            JSONObject data = new JSONObject();

            for (Organization org : orgList) {

                JSONObject orgJSON = new JSONObject();
                orgJSON.put("org_id", org.getOrg_id());
                orgJSON.put("orgname", org.getOrgname());
                orgJSON.put("orgtype", org.getOrgtype());
                orgJSON.put("email", org.getEmail());
                orgJSON.put("phonenumber", org.getPhonenumber());
                orgJSON.put("website_url", org.getWebsite_url());
                orgJSON.put("logolink", org.getLogolink());
                orgJSON.put("need_password_reset", org.getNeed_password_reset());
                orgJSON.put("active_status", org.getActive_status());
                orgJSON.put("is_admin", org.getIs_admin());
                orgJSON.put("chat_id", org.getChat_id());
                orgJSON.put("chat_token", org.getChat_token());
                orgJSON.put("full_names", org.getFull_names());
                orgJSON.put("designation", org.getDesignation());
                orgJSON.put("created_date", org.getTrx_date());
                orgJSON.put("consent", org.getConsent());
                orgJSON.put("consent_details", org.getConsent_details());

                JSONArray countriesArray = new JSONArray();

                for (OrgCountry country : org.getOrgCountries()) {
                    JSONObject countriesJSON = new JSONObject();
                    countriesJSON.put("cty_id", country.getCountry().getCty_id());
                    countriesJSON.put("country_name", country.getCountry().getCountry_name());

                    countriesArray.add(countriesJSON);
                }
                orgJSON.put("countries", countriesArray);

                JSONArray sdgsArray = new JSONArray();

                for (OrgSDG sdg : org.getOrgSdg()) {
                    JSONObject sdgsJSON = new JSONObject();
                    sdgsJSON.put("sdg_id", sdg.getSdg().getSdg_id());
                    sdgsJSON.put("sdg_name", sdg.getSdg().getSdg_name());

                    sdgsArray.add(sdgsJSON);
                }
                orgJSON.put("sdgs", sdgsArray);

                //Add focus areas list
                JSONArray focusAreaArray = new JSONArray();

                for (OrgFocusArea orgFocusArea : org.getOrgFocusAreas()) {
                    JSONObject focusAreasJSON = new JSONObject();
                    focusAreasJSON.put("fa_id", orgFocusArea.getFocusArea().getFa_id());
                    focusAreasJSON.put("focus_name", orgFocusArea.getFocusArea().getFocus_name());

                    focusAreaArray.add(focusAreasJSON);
                }
                orgJSON.put("focusareas", focusAreaArray);

                jArray.add(orgJSON);

                jsonObj.put("data", jArray);
            }

            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }
    }

    @GetMapping("/organizations/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Optional<Organization> org = organizationRepository.findById(id);

        if (!org.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");

            JSONObject data = new JSONObject();
            data.put("org_id", org.get().getOrg_id());
            data.put("orgname", org.get().getOrgname());
            data.put("orgtype", org.get().getOrgtype());
            data.put("email", org.get().getEmail());
            data.put("phonenumber", org.get().getPhonenumber());
            data.put("website_url", org.get().getWebsite_url());
            data.put("logolink", org.get().getLogolink());
            data.put("need_password_reset", org.get().getNeed_password_reset());
            data.put("active_status", org.get().getActive_status());
            data.put("is_admin", org.get().getIs_admin());
            data.put("chat_id", org.get().getChat_id());
            data.put("chat_token", org.get().getChat_token());
            data.put("full_names", org.get().getFull_names());
            data.put("designation", org.get().getDesignation());
            data.put("created_date", org.get().getTrx_date());
            data.put("consent", org.get().getConsent());
            data.put("consent_details", org.get().getConsent_details());

            //Add countries list
            JSONArray countriesArray = new JSONArray();

            for (OrgCountry country : org.get().getOrgCountries()) {
                JSONObject orgJSON = new JSONObject();
                orgJSON.put("cty_id", country.getCountry().getCty_id());
                orgJSON.put("country_name", country.getCountry().getCountry_name());

                countriesArray.add(orgJSON);
            }
            data.put("countries", countriesArray);

            //Add sdgs list
            JSONArray sdgsArray = new JSONArray();

            for (OrgSDG sdg : org.get().getOrgSdg()) {
                JSONObject orgJSON = new JSONObject();
                orgJSON.put("sdg_id", sdg.getSdg().getSdg_id());
                orgJSON.put("sdg_name", sdg.getSdg().getSdg_name());

                sdgsArray.add(orgJSON);
            }
            data.put("sdgs", sdgsArray);

            //Add focus areas list
            JSONArray focusAreaArray = new JSONArray();

            for (OrgFocusArea orgFocusArea : org.get().getOrgFocusAreas()) {
                JSONObject orgJSON = new JSONObject();
                orgJSON.put("fa_id", orgFocusArea.getFocusArea().getFa_id());
                orgJSON.put("focus_name", orgFocusArea.getFocusArea().getFocus_name());

                focusAreaArray.add(orgJSON);
            }
            data.put("focusareas", focusAreaArray);

            jsonObj.put("data", data);
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @DeleteMapping("/organizations/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!organizationRepository.findById(id).isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {
            organizationRepository.deActivateOrg("2", id);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Organization succesfully deactivated");

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

    }

    @PatchMapping("/organizations/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody Organization org) {

        Organization organization = organizationRepository.findById(id).orElse(null);
        if (organization.equals(null)) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "Organization not found");

            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

        boolean needUpdate = false;

        if (StringUtils.hasLength(org.getOrgname())) {
            organization.setOrgname(org.getOrgname());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getEmail())) {
            organization.setEmail(org.getEmail());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getPhonenumber())) {
            organization.setPhonenumber(org.getPhonenumber());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getWebsite_url())) {
            organization.setWebsite_url(org.getWebsite_url());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getOrgtype())) {
            organization.setOrgtype(org.getOrgtype());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getChat_id())) {
            organization.setChat_id(org.getChat_id());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getChat_token())) {
            organization.setChat_token(org.getChat_token());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getFull_names())) {
            organization.setFull_names(org.getFull_names());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getDesignation())) {
            organization.setDesignation(org.getDesignation());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getConsent())) {
            organization.setConsent(org.getConsent());
            needUpdate = true;
        }

        if (StringUtils.hasLength(org.getConsent_details())) {
            organization.setConsent_details(org.getConsent_details());
            needUpdate = true;
        }

        if (needUpdate) {
            organizationRepository.save(organization);
        }

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("responseCode", "200");
        jsonObj.put("responseMessage", "Organization successfully updated");

        return new ResponseEntity<>(jsonObj, HttpStatus.OK);
    }

    @RequestMapping(value = "/organizations/all", method = RequestMethod.GET)
    public ResponseEntity<Object> getOrganizations() {
        List<Organization> orgList = (List<Organization>) organizationRepository.findAll();

        if (orgList.isEmpty()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "404");
            jsonObj.put("responseMessage", "No Organization found");

            return new ResponseEntity<>(jsonObj, HttpStatus.NOT_FOUND);
        } else {

            JSONObject jsonObj = new JSONObject();

            JSONArray jArray = new JSONArray();

            JSONObject data = new JSONObject();

            for (Organization org : orgList) {

                JSONObject orgJSON = new JSONObject();
                orgJSON.put("org_id", org.getOrg_id());
                orgJSON.put("orgname", org.getOrgname());
                orgJSON.put("orgtype", org.getOrgtype());
                orgJSON.put("email", org.getEmail());
                orgJSON.put("phonenumber", org.getPhonenumber());
                orgJSON.put("website_url", org.getWebsite_url());
                orgJSON.put("logolink", org.getLogolink());
                orgJSON.put("need_password_reset", org.getNeed_password_reset());
                orgJSON.put("active_status", org.getActive_status());
                orgJSON.put("is_admin", org.getIs_admin());
                orgJSON.put("chat_id", org.getChat_id());
                orgJSON.put("chat_token", org.getChat_token());
                orgJSON.put("full_names", org.getFull_names());
                orgJSON.put("designation", org.getDesignation());
                orgJSON.put("created_date", org.getTrx_date());
                orgJSON.put("consent", org.getConsent());
                orgJSON.put("consent_details", org.getConsent_details());

                JSONArray countriesArray = new JSONArray();

                for (OrgCountry country : org.getOrgCountries()) {
                    JSONObject countriesJSON = new JSONObject();
                    countriesJSON.put("cty_id", country.getCountry().getCty_id());
                    countriesJSON.put("country_name", country.getCountry().getCountry_name());
                    countriesArray.add(countriesJSON);
                }
                orgJSON.put("countries", countriesArray);

                JSONArray sdgsArray = new JSONArray();

                for (OrgSDG sdg : org.getOrgSdg()) {
                    JSONObject sdgsJSON = new JSONObject();
                    sdgsJSON.put("sdg_id", sdg.getSdg().getSdg_id());
                    sdgsJSON.put("sdg_name", sdg.getSdg().getSdg_name());
                    sdgsArray.add(sdgsJSON);
                }
                orgJSON.put("sdgs", sdgsArray);

                //Add focus areas list
                JSONArray focusAreasArray = new JSONArray();

                for (OrgFocusArea orgFocusArea : org.getOrgFocusAreas()) {
                    JSONObject focusAreasJSON = new JSONObject();
                    focusAreasJSON.put("fa_id", orgFocusArea.getFocusArea().getFa_id());
                    focusAreasJSON.put("focus_name", orgFocusArea.getFocusArea().getFocus_name());
                    focusAreasArray.add(focusAreasJSON);
                }
                orgJSON.put("focusareas", focusAreasArray);

                jArray.add(orgJSON);

                jsonObj.put("data", jArray);
            }

            jsonObj.put("responseCode", "200");
            jsonObj.put("responseMessage", "Success");
            return new ResponseEntity<>(jsonObj, HttpStatus.OK);
        }

//        return new ResponseEntity<List<Organization>>(list, HttpStatus.OK);
    }

    @PostMapping("/organizations/login")
    public ResponseEntity<Object> loginUser(@RequestBody Organization org) {

        Optional<Organization> organization = organizationRepository.findUserByUsernameAndPassword(org.getUsername(), passwordEncoder.get_SHA_512_SecurePassword(org.getPassword()));

        if (!organization.isPresent()) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("responseCode", "401");
            jsonObj.put("responseMessage", "Invalid login credentials");

            return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
        } else {

            if (organization.get().getActive_status().equals("0")) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("responseCode", "401");
                jsonObj.put("responseMessage", "Organization hasn't been activated yet");

                return new ResponseEntity<>(jsonObj, HttpStatus.UNAUTHORIZED);
            } else {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("responseCode", "200");
                jsonObj.put("responseMessage", "Login Successful");

                JSONObject data = new JSONObject();

                data.put("org_id", organization.get().getOrg_id());
                data.put("org_id", organization.get().getOrg_id());
                data.put("orgname", organization.get().getOrgname());
                data.put("email", organization.get().getEmail());
                data.put("logolink", organization.get().getLogolink());
                data.put("orgtype", organization.get().getOrgtype());
                data.put("is_admin", organization.get().getIs_admin());
                data.put("chat_id", organization.get().getChat_id());
                data.put("chat_token", organization.get().getChat_token());
                data.put("need_password_reset", organization.get().getNeed_password_reset());
                data.put("full_names", organization.get().getFull_names());
                data.put("designation", organization.get().getDesignation());
                data.put("created_date", organization.get().getTrx_date());
                data.put("consent", organization.get().getConsent());
                data.put("consent_details", organization.get().getConsent_details());
                jsonObj.put("data", data);
                return new ResponseEntity<>(jsonObj, HttpStatus.OK);
            }
        }

    }

}
