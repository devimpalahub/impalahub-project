/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

/**
 *
 * @author JNWARUI
 */
@Entity
@Table(name = "tbl_sdgs")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SDG {

    //@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long sdg_id;
    
    private String sdg_name;

    public Long getSdg_id() {
        return sdg_id;
    }

    public void setSdg_id(Long sdg_id) {
        this.sdg_id = sdg_id;
    }

    public String getSdg_name() {
        return sdg_name;
    }

    public void setSdg_name(String sdg_name) {
        this.sdg_name = sdg_name;
    }

}
