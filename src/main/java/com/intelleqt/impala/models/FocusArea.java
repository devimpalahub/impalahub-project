/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

/**
 *
 * @author SMWAURA1
 */
import java.sql.Timestamp;
import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tbl_focus_areas")

public class FocusArea {
    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Id
    private Long fa_id;
    private String focus_name;
    @CreationTimestamp
    private Timestamp trx_date;
    
    public Long getFa_id() {
        return fa_id;
    }

    public void setFa_id(Long fa_id) {
        this.fa_id = fa_id;
    }

    public String getFocus_name() {
        return focus_name;
    }

    public void setFocus_name(String focus_name) {
        this.focus_name = focus_name;
    }

    public Timestamp getTrx_date() {
        return trx_date;
    }

    public void setTrx_date(Timestamp trx_date) {
        this.trx_date = trx_date;
    }

    
}
