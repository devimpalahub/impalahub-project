/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.controllers;

import com.impalahub.models.SendEmail;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JNWARUI
 */
@RestController
public class SendEmailController extends BaseController {

    @PostMapping(value = "/sendemail")
    public ResponseEntity<Object> sendMail(@RequestBody SendEmail sendEmail) {
        emailSender.sendMailWithHTML(sendEmail.getFromEmail(), sendEmail.getFromName(), sendEmail.getTo(), sendEmail.getSubject(), sendEmail.getBody());
        //Send Json reply to client
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("responseCode", "200");
        jsonObj.put("responseMessage", "Success");
        return new ResponseEntity<>(jsonObj, HttpStatus.OK);
    }
}
