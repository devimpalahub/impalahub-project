/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

import com.intelleqt.impala.models.SDG;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author SMWAURA1
 */
public interface SDGRepository extends JpaRepository<SDG, Long> {

}
