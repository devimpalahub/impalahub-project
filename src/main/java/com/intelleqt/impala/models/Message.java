/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;

/**
 *
 * @author SMWAURA1
 */
import java.util.List;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author SMWAURA1
 */
@Entity
@Table(name = "tbl_messages")
@Data

public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long msg_id;

    private Long from_org_id;
    private Long to_org_id;
    private String subject;
    private String message;
    private Long status_read;

    @CreationTimestamp
    @JsonIgnore
    private Timestamp trx_date;
}
