/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intelleqt.impala.repository;

import com.intelleqt.impala.models.Message;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author SMWAURA1
 */
public interface MessageRepository extends JpaRepository<Message, Long>  {
    
    @Query("SELECT u FROM Message u WHERE u.to_org_id = ?1 ORDER BY u.trx_date desc")
    List<Message> findAllMessagesByID(Long orgId);
    @Query("SELECT u FROM Message u WHERE u.to_org_id = ?1 and u.status_read=?2 ORDER BY u.trx_date desc")
    List<Message> findMessageStatusByID(Long orgId, Long status);
    @Transactional
    @Modifying
    @Query("UPDATE Message u set u.status_read = 1 where u.msg_id=?1")
    void markAsRead(Long msg_id);
}
